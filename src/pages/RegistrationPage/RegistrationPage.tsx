import { useMutation } from "@apollo/client";
import { FC } from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import Logo from "../../components/Logo/Logo";
import { REGISTRATION } from "../../graphql/mutation";
import { useValidationRegistrPage } from "../../hooks/useValidationRegistrPage";
import styles from "./registrationPage.module.scss";

const RegistrationPage: FC = () => {
  const [serverError, setServerError] = useState("");
  const navigate = useNavigate();

  const [registration, { loading, error }] = useMutation(REGISTRATION, {
    onError: (e) => {
      console.log(e.graphQLErrors[0].message);
      setServerError(e.graphQLErrors[0].message);
    },
    onCompleted(data) {
      navigate("/login");
    },
  });
  const { form, validationMethods, errors, isError } =
    useValidationRegistrPage();
  const {
    login,
    email,
    password,
    avatar = "",
    src = "",
    repeatPassword,
  } = form;
  useEffect(() => {
    if (serverError !== "") {
      setServerError("");
    }
  }, [email, login]);

  const onClick = () => {
    registration({ variables: { avatar: src, email, password, login } });
  };

  return (
    <div className={styles.registrationPage}>
      <div className={styles.title}>
        <p>Registration</p>
      </div>
      <div className={styles.registration_inputs}>
        <Input
          error={(errors.login && errors.login) || (error && serverError)}
          title="Login"
          name="login"
          value={login}
          onChange={validationMethods.loginValidation}
        />
        <Input
          error={(errors.email && errors.email) || (error && serverError)}
          title="Email"
          name="email"
          value={email}
          onChange={validationMethods.emailValidation}
        />
        <Input
          error={errors.password && errors.password}
          title="Password"
          name="password"
          type="password"
          value={password}
          onChange={validationMethods.passwordValidation}
        />
        <Input
          error={errors.repeatPassword && errors.repeatPassword}
          title="Repeat password"
          name="repeatPassword"
          type="password"
          value={repeatPassword}
          onChange={validationMethods.repeatPasswordValidation}
        />
      </div>
      <div className={styles.logo_title}>
        <p>logo</p>
      </div>
      <Logo
        avatar={avatar}
        src={src}
        onChange={validationMethods.avatarValidation}
      />
      <div className={styles.register_btn}>
        <div className={styles.button}>
          <Button
            disabled={isError || loading}
            onClick={onClick}
            title="Register"
          />
        </div>
      </div>
    </div>
  );
};

export default RegistrationPage;
