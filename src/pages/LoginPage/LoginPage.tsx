import { useLazyQuery } from "@apollo/client";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import Button from "../../components/Button/Button";
import Input from "../../components/Input/Input";
import { useAuthContext } from "../../hooks/AuthContext";
import { SIGN_IN } from "../../graphql/query";
import styles from "./loginPage.module.scss";
import { useValidationLoginPage } from "../../hooks/useValidationLoginPage";
import { FC } from "react";
import { reconnectSubscriptionClient } from '../../graphql';

const LoginPage: FC = () => {
  const [serverError, setServerError] = useState("");
  const { form, validationMethods, isError, errors } = useValidationLoginPage();

  const context = useAuthContext();
  const navigate = useNavigate();

  const [signIn, {loading, error }] = useLazyQuery(SIGN_IN, {
    onError: (e) => {
      console.log(e.graphQLErrors[0].message);
      setServerError(e.graphQLErrors[0].message);
    },
    onCompleted: (data)=> {
      const user = data.signIn.user;
      localStorage.setItem("token", data.signIn.token);
      reconnectSubscriptionClient()
      context.setUser({
        User: {
          login: user.login,
          id: user.id,
          avatar: user.avatar,
          email: user.email,
        },
        isAuth: true,
      });

      navigate("/");

    },
  });

  useEffect(() => {
    if (serverError !== "") {
      setServerError("");
    }
  }, [form.email, form.password]);

  const onClick = () => {
    signIn({
      variables: { email: form.email, password: form.password },
    });

  };

  return (
    <div className={styles.loginPage}>
      <div className={styles.title}>
        <p>Welcome</p>
      </div>
      <div className={styles.login_inputs}>
        <Input
          error={(errors.email && errors.email) || (error && serverError)}
          title="Email"
          name="email"
          value={form.email}
          onChange={validationMethods.emailValidation}
        />

        <Input
          error={(errors.password && errors.password) || (error && serverError)}
          title="Password"
          name="password"
          type="password"
          value={form.password}
          onChange={validationMethods.passwordValidation}
        />
      </div>

      <div className={styles.grid}>
        <Link to="/registration">Registration</Link>
        <Button disabled={isError || loading} onClick={onClick} title="Login" />
      </div>
    </div>
  );
};

export default LoginPage;
