import React, { useState } from "react";
import { FC } from "react";
import Chat from "../../components/Chat/Chat";
import Rooms from "../../components/Rooms/Rooms";
import { IUser } from "../../hooks/AuthContext";
import useMessagesState from "../../hooks/useMessagesState";
import styles from "./chatPage.module.scss";

export interface IMessage {
  convId?: string | number;
  description: string;
  date: Date | string;
  user?: IUser;
}

const ChatPage: FC = () => {
  const [activeRoomId, setActiveRoomId] = useState<string | "">("");
  const { messages, messagesLoading } = useMessagesState(activeRoomId);

  return (
    <div className={styles.chatPage}>
      <Rooms activeRoomId={activeRoomId} setActiveRoomId={setActiveRoomId} />
      {messagesLoading ? (
        <p>Loading...</p>
      ) : (
        <Chat
          messages={messages}
          activeRoomId={activeRoomId}
          messagesLoading={messagesLoading}
        />
      )}
    </div>
  );
};

export default ChatPage;
