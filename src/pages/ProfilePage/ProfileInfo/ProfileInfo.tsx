import React, { useMemo } from "react";
import { FC } from "react";
import Avatar from "../../../components/Avatar/Avatar";
import { IUser } from "../../../hooks/AuthContext";
import formatDataFunction from "../../../utils/formatDataFunction";
import styles from "./profileInfo.module.scss";

interface IProfileInfo {
  user?: IUser | null;
}

const ProfileInfo: FC<IProfileInfo> = ({ user }) => {
  const decodedToken = useMemo(() => {
    const token = localStorage.getItem("token")!;
    let tokenDate;
    try {
      tokenDate = new Date(JSON.parse(atob(token.split(".")[1])).iat * 1000);
    } catch (error) {
      console.log(`Token error: ${error}`);
    }
    if (!tokenDate) return;
    const { day, month, year, hours, minutes } = formatDataFunction(tokenDate);
    return `${day}.${month}.${year} ${hours}:${minutes}`;
  }, []);

  return (
    <div className={`${styles.profileInfo}`}>
      <div className={`${styles.profileInfo_avatar}`}>
        <Avatar size="large" src={user?.avatar} />
      </div>
      <div className={`${styles.profileInfo_userInfo}`}>
        <div className={`${styles.profileInfo_login}`}>
          <p>{user?.login}</p>
        </div>
        <div className={`${styles.profileInfo_email}`}>
          <p>{user?.email}</p>
        </div>
        <div className={`${styles.profileInfo_tokenInfo}`}>
          <p>{`Token expair at:
          ${decodedToken}`}</p>
        </div>
      </div>
    </div>
  );
};

export default ProfileInfo;
