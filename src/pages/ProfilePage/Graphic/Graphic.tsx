import Chart, { BubbleDataPoint, ScatterDataPoint } from "chart.js/auto";
import React, { useEffect, useMemo, useRef } from "react";
import { FC } from "react";
import createGraphConfig from "../../../utils/сreateGraphConfig";
import styles from "./graphic.module.scss";

interface IGrapgic {
  loading?: boolean;
  statistics: Array<number | ScatterDataPoint | BubbleDataPoint | null>;
}

const Grapgic: FC<IGrapgic> = ({ loading, statistics }) => {
  const chartRef = useRef<HTMLCanvasElement>(null);
  const graphConfig = useMemo(
    () =>
      createGraphConfig({
        statistics,
      }),
    [statistics]
  );
  useEffect(() => {
    if (graphConfig) {
      const chart = new Chart(chartRef.current!, graphConfig);
      return () => {
        chart.destroy();
      };
    }
  }, [chartRef.current, graphConfig]);

  if (loading) return <p>Loading...</p>;
  return <canvas className={styles.graphicChart} ref={chartRef} />;
};

export default Grapgic;
