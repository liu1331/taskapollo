import { useQuery } from "@apollo/client";
import { FC, useEffect, useState } from "react";
import Button from "../../components/Button/Button";
import { GET_MESSAGES_STATISTICS } from "../../graphql/query";
import { useAuthContext } from "../../hooks/AuthContext";
import Graphic from "./Graphic/Graphic";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import styles from "./profilePage.module.scss";
import { BubbleDataPoint, ScatterDataPoint } from "chart.js/auto";

export interface IStatistics {
  count: number;
  date?: string;
}
const ProfilePage: FC = () => {
  const [statistics, setStatistics] = useState<
    Array<number | ScatterDataPoint | BubbleDataPoint | null>
  >([]);
  const { user, logout } = useAuthContext();

  const { data: messagesStatistics, loading } = useQuery(
    GET_MESSAGES_STATISTICS,
    {
      fetchPolicy: "network-only",
    }
  );
  useEffect(() => {
    if (messagesStatistics) {
      setStatistics(
        messagesStatistics.getMessageStatistics.map(
          (statistic: IStatistics) => statistic.count
        )
      );
    }
  }, [messagesStatistics]);

  if (loading) return <p>Loading...</p>;
  return (
    <div className={`${styles.profilePage}`}>
      <div className={`${styles.grid_container}`}>
        <div className={`${styles.profileTitle}`}>
          <p>Profile</p>
        </div>
        <ProfileInfo user={user.User} />
        <div className={`${styles.profileGraphic}`}>
          <Graphic statistics={statistics} loading={loading} />
        </div>
        <div className={`${styles.profileButton}`}>
          <div className={`${styles.button}`}>
            <Button onClick={logout} title="Logout" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfilePage;
