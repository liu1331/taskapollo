import { useEffect, useState } from "react";
import {
  emailValidation,
  passwordValidation,
} from "../utils/validationFunctions";
import useDebounce from "./useDebounce";

interface IForm {
  [key: string]: string | "";
}

export function useValidationRegistrPage() {
  const [form, setForm] = useState<IForm>({});
  const [errors, setErrors] = useState<IForm>({});
  const [isError, setIsError] = useState<boolean>(false);

  const setError = (name: string, errData: string) =>
    setErrors((prevState) => ({ ...prevState, [name]: errData }));
  const debounceSetErrors = useDebounce(setError, 500);
  const debounceSetSrc = useDebounce(
    (value: string) => setForm((prevState) => ({ ...prevState, src: value })),
    500
  );
  const getValidationFunctions = (...validationFunctions: any) => {
    if (validationFunctions.length > 0) {
      const errors = validationFunctions.filter(
        (validationFunction: any) => validationFunction.errData !== ""
      );
      if (errors.length > 0) {
        return errors[0];
      } else {
        return { errData: "" };
      }
    } else {
      return { errData: "" };
    }
  };
  const setValidValue = (name: string, value: string, ...callbacks: any) => {
    setForm((prevState) => ({ ...prevState, [name]: value }));
    const { errData } = getValidationFunctions(...callbacks);
    debounceSetErrors(name, errData);
  };

  useEffect(() => {
    var checkErr = Object.values(errors).filter(function (err) {
      return err !== "";
    });
    checkErr.length > 0 ? setIsError(true) : setIsError(false);
  }, [errors]);

  useEffect(() => {
    if (!form.password || !form.repeatPassword) return;
    if (form.password === form.repeatPassword) {
      setErrors((prevState: any) => ({
        ...prevState,
        password: "",
        repeatPassword: "",
      }));
    } else {
      debounceSetErrors("repeatPassword", "Пароль не совпадает");
    }
  }, [form.password, form.repeatPassword]);

  const validationMethods = {
    emailValidation: (value: string, name: string) => {
      setValidValue(name, value, emailValidation.validEmail(value));
    },
    passwordValidation: (value: string, name: string) => {
      setValidValue(name, value, passwordValidation.passwordIsEmpty(value));
    },
    loginValidation: (value: string, name: string) => {
      setValidValue(name, value);
    },

    repeatPasswordValidation: (value: string, name: string) => {
      setValidValue(name, value, passwordValidation.passwordIsEmpty(value));
    },
    avatarValidation: (value: string, name: string) => {
      setValidValue(name, value);
      debounceSetSrc(value);
    },
  };

  return {
    form,
    validationMethods,
    errors,
    isError,
  };
}
