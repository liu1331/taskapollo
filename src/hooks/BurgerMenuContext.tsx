
import { createContext, useState, useContext, ReactNode } from "react";


interface IBurgerMenuContext {
  isOpenBurger: boolean;
  setIsOpenBurger: (arg: boolean) => void;
}
const BurgerMenuContext = createContext<IBurgerMenuContext>({isOpenBurger:true, setIsOpenBurger:() => {}} );
type Props = {
  children: ReactNode;
};
export const BurgerProvider = (props: Props) => {
  const [isOpenBurger, setIsOpenBurger] = useState<boolean>(true);



  return (
    <BurgerMenuContext.Provider value={{ isOpenBurger, setIsOpenBurger }}>
      {props.children}
    </BurgerMenuContext.Provider>
  );
};
export const useBurgerMenuContext = () => useContext(BurgerMenuContext);
