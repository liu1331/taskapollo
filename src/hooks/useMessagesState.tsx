import { useLazyQuery, useSubscription } from "@apollo/client";
import React, { useEffect, useState } from "react";
import { GET_ALL_MESSAGES } from "../graphql/query";
import { MESSAGE_ADDED } from "../graphql/suscription";
import { IMessage } from "../pages/ChatPage/ChatPage";

interface IUseMessagesState {
  messages: IMessage[];
  setMessages: (state: (prevState: IMessage[]) => IMessage[]) => void;
  messagesLoading: boolean;
}

const useMessagesState = (activeRoomId: string): IUseMessagesState => {
  const [messages, setMessages] = useState<IMessage[]>([]);

  const [getAllMessages, { loading: messagesLoading, data: messagesData }] =
    useLazyQuery(GET_ALL_MESSAGES, {
      fetchPolicy: "network-only",
    });

  const getFutureDate = (date: Date | string) => {
    const newDate = new Date(date);
    return new Date(
      newDate.setFullYear(newDate.getFullYear() + 1)
    ).toISOString();
  };
  const lastMessageDate =
    messages.length > 0
      ? getFutureDate(messages[messages.length - 1].date)
      : "";

  const { data: messageAdded, error: messageAddedError } = useSubscription(
    MESSAGE_ADDED,
    {
      variables: {
        date: lastMessageDate,
      },
    }
  );
  useEffect(() => {
    if (activeRoomId) {
      getAllMessages({ variables: { convId: Number(activeRoomId) } });
    }
  }, [activeRoomId]);
  useEffect(() => {
    if (messagesData) {
      const messages = messagesData.getAllMessages.map((message: IMessage) => {
        return {
          description: message.description,
          date: message.date,
          user: { ...message.user },
        };
      });
      setMessages([...messages]);
    }
  }, [messagesData]);

  useEffect(() => {
    if (messageAddedError) console.log(messageAddedError);
    if (messageAdded && activeRoomId) {
      const newMessages = messageAdded.messageAdded.filter(
        (newMessage: IMessage) => {
          if (Number(activeRoomId) === Number(newMessage.convId)) {
            return {
              description: newMessage.description,
              date: newMessage.date,
              user: newMessage.user,
            };
          } else {
            return false;
          }
        }
      );
      if (newMessages.length > 0) {
        setMessages((prevState) => [...prevState, ...newMessages]);
      }
    }
  }, [messageAddedError, messageAdded, activeRoomId]);
  return { messages, setMessages, messagesLoading };
};

export default useMessagesState;
