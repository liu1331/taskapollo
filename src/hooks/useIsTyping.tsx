import { useLazyQuery } from "@apollo/client/react/hooks/useLazyQuery";
import { useSubscription } from "@apollo/client/react/hooks/useSubscription";
import throttle from "lodash/throttle";
import { useCallback } from "react";
import { TYPING_USER } from "../graphql/query";
import { IS_TYPING } from "../graphql/suscription";
import { IUser } from "./AuthContext";

export interface IIsTyping {
  convId?: number;
  date?: Date | string;
  user: IUser;
}
interface IData {
  typingUsers: ITypingUsers | undefined;
  typing: (arg: ITyping) => void;
}
interface ITyping {
  variables: IVariables;
}
interface IVariables {
  convId: number;
}
export interface ITypingUsers {
  isTyping: IIsTyping[];
}

export const useIsTyping = (activeRoomId: string | number): IData => {
  const [typingUserData] = useLazyQuery(TYPING_USER, {
    variables: {
      convId: activeRoomId && +activeRoomId,
    },
    fetchPolicy: "network-only",
    onError: (err) => {
      console.log(err);
    },
  });

  const typing = useCallback(throttle(typingUserData, 2000), []);
  const { data: typingUsers } = useSubscription(IS_TYPING, {
    variables: {
      convId: activeRoomId && +activeRoomId,
    },
  });

  return { typingUsers, typing };
};
