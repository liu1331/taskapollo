import { useEffect, useState } from "react";
import {
  emailValidation,
  passwordValidation,
} from "../utils/validationFunctions";
import useDebounce from "./useDebounce";

interface IForm {
  [key: string]: string | "";
}

export function useValidationLoginPage() {
  const [form, setForm] = useState<IForm>({});
  const [errors, setErrors] = useState<IForm>({});
  const [isError, setIsError] = useState<boolean>(false);

  const setError = (name: string, errData: string) =>
    setErrors((prevState) => ({ ...prevState, [name]: errData }));
  const debounceSetErrors = useDebounce(setError, 500);
  const callFuncArr = (...args: any) => {
    if (args.length > 0) {
      const res = args.filter((err: any) => err.errData !== "");
      if (res.length > 0) {
        return res[0];
      } else {
        return { errData: "" };
      }
    } else {
      return args[0].errData;
    }
  };
  const setValidValue = (name: string, value: string, ...callbacks: any) => {
    setForm((prevState) => ({ ...prevState, [name]: value }));
    const { errData } = callFuncArr(...callbacks);
    debounceSetErrors(name, errData);
  };
  useEffect(() => {
    var checkErr = Object.values(errors).filter(function (err) {
      return err !== "";
    });
    checkErr.length > 0 ? setIsError(true) : setIsError(false);
  }, [errors]);

  const validationMethods = {
    emailValidation: (value: string, name: string) => {
      setValidValue(name, value, emailValidation.validEmail(value));
    },
    passwordValidation: (value: string, name: string) => {
      setValidValue(name, value, passwordValidation.passwordIsEmpty(value));
    },
  };

  return {
    form,
    validationMethods,
    errors,
    isError,
  };
}
