import { useQuery } from "@apollo/client";
import { createContext, useState, useContext, ReactNode } from "react";
import { ME } from "../graphql/query";
export interface IUser {
  login: string;
  id: string;
  avatar?: string;
  email: string;
}

interface IAuthContext {
  user: IAuth;
  setUser: (arg: IAuth) => void;
  logout: () => void;
}
const AuthContext = createContext<IAuthContext>({
  user: { User: { login: "", id: "", avatar: "", email: "" }, isAuth: false },
  setUser: () => {},
  logout: () => {},
});
type Props = {
  children: ReactNode;
};
interface IAuth {
  User: IUser | null;
  isAuth: boolean;
}
export const AuthProvider = (props: Props) => {
  const [user, setUser] = useState<IAuth>({
    User: null,
    isAuth: false,
  });
  const logout = () => {
    localStorage.removeItem("token");
    setUser({
      User: null,
      isAuth: false,
    });
  };

  const { loading } = useQuery(ME, {
    onError: (error) => {
      console.log(error.graphQLErrors[0]);
      localStorage.removeItem("token");
    },
    onCompleted(data) {
      const { id, login, email, avatar } = data.me.user;
      setUser((prevState) => ({
        ...prevState,
        User: { id, login, email, avatar },
        isAuth: true,
      }));
    },
  });

  if (loading) return null;

  return (
    <AuthContext.Provider value={{ user, setUser, logout }}>
      {props.children}
    </AuthContext.Provider>
  );
};
export const useAuthContext = () => useContext(AuthContext);
