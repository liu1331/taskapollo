import { useEffect, useState } from "react";
interface IState {
  width: string | number | null;
  height: string | number | null;
}
export function useWindowSize(isOpen: boolean) {
  const [windowSize, setWindowSize] = useState<IState>({
    width: null,
    height: null,
  });
  useEffect(() => {
    if (!isOpen) return;
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
  }, [isOpen]);
  return windowSize;
}
