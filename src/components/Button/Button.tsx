import { FC } from "react";
import styles from "./button.module.scss";

interface BtnProps {
  onClick?: () => void;
  styleType?: "primary" | "secondary" | "";
  classNameBgImage?: string;
  disabled?: boolean;
  title?: string;
}
const Button: FC<BtnProps> = ({
  onClick,
  styleType = "primary",
  disabled = false,
  title,
  classNameBgImage,
}) => {
  return (
    <>
      <button
        className={`${styles.btn} ${classNameBgImage} ${styles[styleType]}`}
        onClick={() => onClick && onClick()}
        disabled={disabled}
      >
        {title}
      </button>
    </>
  );
};

export default Button;
