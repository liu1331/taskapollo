import React, { ChangeEvent, KeyboardEvent } from "react";
import { FC } from "react";
import styles from "./textarea.module.scss";

interface TextareaProps {
  value?: string;
  onChange?: (value: string) => void;
  onKeyDown?: (e: KeyboardEvent<HTMLTextAreaElement>) => void;
  name?: string;
  placeholder?: string;
  rows?: number;
  cols?: number;
}

const Textarea: FC<TextareaProps> = ({
  value = "",
  onChange,
  placeholder,
  name,
  rows = 2,
  cols = 1,
  onKeyDown,
}) => {
  return (
    <div className={styles.TextareaForm}>
      <div className={styles.Textarea_inner}>
        <textarea
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={(e: ChangeEvent<HTMLTextAreaElement>) => {
            onChange && onChange(e.target.value);
          }}
          onKeyDown={(e: KeyboardEvent<HTMLTextAreaElement>) =>
            onKeyDown && onKeyDown(e)
          }
          rows={rows}
          cols={cols}
        />
      </div>
    </div>
  );
};

export default Textarea;
