import { useMutation } from "@apollo/client";
import React, { FC, KeyboardEvent, useEffect, useRef, useState } from "react";
import { CREATE_MESSAGE } from "../../graphql/mutation";
import { useAuthContext } from "../../hooks/AuthContext";
import { useIsTyping } from "../../hooks/useIsTyping";
import { IMessage } from "../../pages/ChatPage/ChatPage";
import Button from "../Button/Button";
import Message from "../Message/Message";
import TypingUserInfo from "../TypingUserInfo/TypingUserInfo";
import styles from "./chat.module.scss";
import Textarea from "./Textarea/Textarea";

interface IChat {
  activeRoomId: string | number;
  messages?: IMessage[];
  messagesLoading: boolean;
}
const Chat: FC<IChat> = ({ activeRoomId, messages, messagesLoading }) => {
  const [messageText, setMessageText] = useState("");
  const chatMessagesRef = useRef<HTMLDivElement | null>(null);
  const { user } = useAuthContext();
  const { typingUsers, typing } = useIsTyping(activeRoomId);
  const [createMessage] = useMutation(CREATE_MESSAGE);
  const submitMessage = () => {
    if (messageText.trim().split(" ").join("").length > 0) {
      createMessage({
        variables: {
          description: messageText,
          convId: Number(activeRoomId),
        },
      });
    }
    setMessageText("");
  };
  const onChange = (text: string) => {
    setMessageText(text);
  };

  const onKeyDown = (e: KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.altKey || e.ctrlKey) {
      if (e.keyCode === 13) {
        submitMessage();
      }
      return;
    }
    typing({
      variables: {
        convId: Number(activeRoomId),
      },
    });
  };

  useEffect(() => {
    if (!activeRoomId) return;
    function scroll() {
      const messagesDiv = chatMessagesRef.current;
      if (!messagesDiv) return;
      messagesDiv.scrollTop = messagesDiv?.scrollHeight;
    }
    scroll();
  }, [messages]);

  if (messagesLoading) return <p>Loading...</p>;

  return (
    <div className={styles.Chat}>
      <div ref={chatMessagesRef} className={styles.Chat_messages}>
        <div className={styles.Chat_messages_inner}>
          {messages?.map((message, index) => {
            return (
              <Message
                key={index}
                isMyMessage={
                  Number(message?.user?.id) === Number(user.User?.id)
                    ? true
                    : false
                }
                description={message.description}
                user={message.user}
                date={new Date(message.date)}
              />
            );
          })}
        </div>
      </div>
      <div className={styles.isTyping_info}>
        <TypingUserInfo typingUsers={typingUsers} />
      </div>

      {activeRoomId && (
        <div className={styles.Chat_footer}>
          <div className={styles.Chat_footer_input}>
            <Textarea
              value={messageText}
              onChange={(text: string) => onChange(text)}
              onKeyDown={onKeyDown}
              name="message"
            />
          </div>
          <div className={styles.Chat_footer_button}>
            <Button
              classNameBgImage={styles.button_bgImage}
              onClick={() => {
                submitMessage();
              }}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Chat;
