import React, { FC } from "react";
import Avatar from "../Avatar/Avatar";
import Input from "../Input/Input";
import styles from "./logo.module.scss";

interface ILogo {
  avatar?: string;
  onChange?: (value: string, name: string) => void;
  src?: string;
}

const Logo: FC<ILogo> = ({ avatar, src, onChange }) => {
  return (
    <div className={styles.logoContent}>
      <div className={styles.flex}>
        <Avatar src={src} size="large" />
        <div className={styles.logo_input}>
          <Input title="url" name="avatar" value={avatar} onChange={onChange} />
        </div>
      </div>
    </div>
  );
};

export default Logo;
