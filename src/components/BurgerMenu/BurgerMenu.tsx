import React from 'react';
import { useBurgerMenuContext } from '../../hooks/BurgerMenuContext';
import styles from "./burgerMenu.module.scss"

const BurgerMenu = () => {
  const {isOpenBurger,setIsOpenBurger} =useBurgerMenuContext()
  return <div className={`${styles.burgerMenu} ${styles.isOpen}`} onClick={()=> setIsOpenBurger(!isOpenBurger)}>
  <div></div>
  <div></div>
  <div></div>
</div>;
};

export default BurgerMenu;
