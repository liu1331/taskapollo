import { FC } from "react";
import styles from "./avatar.module.scss";

interface AvatarProps {
  src?: string;
  size?: "small" | "large";
}
const Avatar: FC<AvatarProps> = ({ src, size = "small" }) => {
  return (
    <div className={`${styles.avatar} ${styles[size]}`}>
      <img alt="Avatar" src={src} />
    </div>
  );
};

export default Avatar;
