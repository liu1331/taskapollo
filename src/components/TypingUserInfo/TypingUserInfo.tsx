import React, { FC } from "react";
import { useAuthContext } from "../../hooks/AuthContext";
import { IIsTyping, ITypingUsers } from "../../hooks/useIsTyping";
interface ITypingUserInfo {
  typingUsers?: ITypingUsers;
}

const TypingUserInfo: FC<ITypingUserInfo> = ({ typingUsers }) => {
  const { user } = useAuthContext();
  return (
    <>
      {typingUsers && typingUsers.isTyping.length > 1 ? (
        <p>...is typing</p>
      ) : (
        typingUsers &&
        typingUsers.isTyping
          .filter(
            (typingUser: IIsTyping) => typingUser.user.id !== user.User?.id
          )
          .map((typingUser) => (
            <p key={typingUser.user.id}>{typingUser.user.login} is typing...</p>
          ))
      )}
    </>
  );
};

export default TypingUserInfo;
