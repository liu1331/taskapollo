import React, { useRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../hooks/AuthContext";
import Avatar from "../Avatar/Avatar";
import BurgerMenu from '../BurgerMenu/BurgerMenu';
import styles from "./header.module.scss";
import Menu from "./Menu/Menu";

const Header = () => {
  const avatar = useRef<HTMLDivElement | null>(null);
  const { user, logout } = useAuthContext();

  const navigate = useNavigate();
  const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false);
  const logoutOnClick = () => {
    logout();
    setIsOpenMenu(false);
  };
  const {pathname} =useLocation()


  return (
    <div className={styles.header}>
      <div className={styles.header_container}>
        <div className={`${styles.logo_container} ${styles.isOpenBurger}`}>
     {pathname === '/' && (<BurgerMenu/>)}
        <div onClick={() => navigate("/")} className={styles.logo}>
          <p>GQL Chat</p>
        </div>
        </div>

        <div className={styles.item}>
          {user.isAuth ? (
            <div
              ref={avatar}
              onClick={(e) => {
                setIsOpenMenu(!isOpenMenu);
              }}
              className={styles.item_avatar}
            >
              <Avatar size="small" src={user!.User!.avatar} />
            </div>
          ) : (
            <Link to="/login">Login</Link>
          )}
        </div>

        <Menu
          parentElementRef={avatar}
          className={styles.menu_content_width}
          isOpen={isOpenMenu}
          setIsOpenMenu={setIsOpenMenu}
        >
          <Link to="/profile">Profile </Link>
          <p onClick={logoutOnClick}>Logout</p>
        </Menu>
      </div>
    </div>
  );
};

export default Header;
