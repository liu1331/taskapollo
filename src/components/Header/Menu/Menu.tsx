import React, { ReactChild, RefObject, useMemo, useRef } from "react";
import { FC } from "react";
import styles from "./menu.module.scss";
import { useClickOutside } from "../../../hooks/useClickOutside";
import ReactDOM from "react-dom";
import { useWindowSize } from '../../../hooks/useWindowSize';

interface IMenu {
  parentElementRef?: RefObject<HTMLElement>;
  isOpen: boolean;
  setIsOpenMenu: (arg: boolean) => void;
  children?: Array<ReactChild>;
  className?: string;
  sidePosition?: string;
}

const appRoot = document.getElementById("modalRoot")!;

const Menu: FC<IMenu> = ({
  setIsOpenMenu,
  isOpen,
  children,
  className,
  parentElementRef,
  sidePosition = "right",
}) => {
  const menuRef = useRef<HTMLDivElement | null>(null);
  const windowSize = useWindowSize(isOpen);

  const position = useMemo(() => {
    if (parentElementRef?.current && isOpen) {
      const { top, left, height, right } =
        parentElementRef.current.getBoundingClientRect();

      if (sidePosition === "right") {
        return {
          top: `${top + height}px`,
          right: `${Number(windowSize.width) - right}px`,
        };
      } else {
        return {
          top: `${top + height}px`,
          left: `${left}px`,
        };
      }
    }
  }, [windowSize, isOpen]);

  useClickOutside(menuRef, () => setIsOpenMenu(false));
  const arrayChildren = children;
  if (!isOpen) return null;
  return ReactDOM.createPortal(
    <div
      ref={menuRef}
      className={`${styles.menu_wrapper} `}
      style={{ ...position }}
    >
      <div className={`${styles.menu} ${className ? className : ''} `}>
        {React.Children.map(arrayChildren, (child) => {
          return <div className={styles.menu_item}>{child}</div>;
        })}
      </div>
    </div>,
    appRoot
  );
};
export default Menu;
