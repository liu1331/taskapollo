import { FC } from "react";
import styles from "./layout.module.scss";
const Layout: FC = (props) => {
  return (
    <div className={styles.layout}>
      <div className={styles.layout_container}>{props.children}</div>
    </div>
  );
};

export default Layout;
