import React from "react";
import { FC } from "react";
import styles from "./input.module.scss";

interface InputProps {
  value?: string;
  onChange?: (value: string, name: string) => void;
  error?: string;
  title?: string;
  name: string;
  type?: string;
  placeholder?: string;
}

const Input: FC<InputProps> = ({
  value = "",
  onChange,
  placeholder,
  error,
  title,
  name,
  type,
}) => {
  return (
    <div className={styles.inputForm}>
      {title && <div className={styles.inputTitle}>{title}</div>}
      <div className={styles.input_inner}>
        <input
          name={name}
          className={`${styles.input} ${error && styles.inputError}`}
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            onChange && onChange(e.target.value, name);
          }}
        />
      </div>

      {error && <div className={styles.inputErrorText}>{error}</div>}
    </div>
  );
};

export default Input;
