import React, { FC, useEffect, useState } from "react";
import styles from "./room.module.scss";

interface IRoom {
  name: string;
  activeRoomId?: string | null;
  id: string;
  onClick: (id: string) => void;
}

const Room: FC<IRoom> = ({ name, activeRoomId, id, onClick }) => {
  const [isActiveRoom, setIsActiveRoom] = useState<boolean>(false);

  useEffect(() => {
    if (activeRoomId === id) {
      if (isActiveRoom) return;
      setIsActiveRoom(true);
      return;
    }
    if (isActiveRoom) {
      setIsActiveRoom(false);
    }
  }, [activeRoomId, id, isActiveRoom]);

  return (
    <div
      onClick={() => onClick(id)}
      className={`${styles.room} ${isActiveRoom ? styles.active : ""}`}
    >
      {name}
    </div>
  );
};

export default Room;
