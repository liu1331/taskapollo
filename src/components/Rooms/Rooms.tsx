import { useQuery, useSubscription } from "@apollo/client";
import React, { FC, useEffect, useState } from "react";
import { GET_ALL_CONVERSATIONS } from "../../graphql/query";
import { CONVERSATION_ADDED } from "../../graphql/suscription";
import { useBurgerMenuContext } from '../../hooks/BurgerMenuContext';
import AddRoomField from "./AddRoomField/AddRoomField";
import Room from "./Room/Room";
import styles from "./rooms.module.scss";

interface IConversation {
  [key: string]: string;
}
interface IRooms {
  setActiveRoomId: (id: string) => void;
  activeRoomId?: string;
}
const Rooms: FC<IRooms> = ({ setActiveRoomId, activeRoomId }) => {
  const [conversations, setConversations] = useState<IConversation[]>([]);
  const [isActiveAddRoom, setIsActiveAddRoom] = useState(false);
  const { data, loading } = useQuery(GET_ALL_CONVERSATIONS);
  const {isOpenBurger, setIsOpenBurger} = useBurgerMenuContext()

  const { data: conversationAdded, error: conversationAddederror } =
    useSubscription(CONVERSATION_ADDED);

  const onClick = (id: string) => {
    setActiveRoomId && setActiveRoomId(id);
    setIsOpenBurger(false)
  };

  useEffect(() => {
    if (data) {
      setConversations(data.getAllConversations);
      setActiveRoomId && setActiveRoomId(data.getAllConversations[0].id);
    }
  }, [data]);
  useEffect(() => {
    if (conversationAddederror) console.log(conversationAddederror);
    if (conversationAdded) {
      setConversations(conversationAdded.conversationAdded);
    }
  }, [conversationAdded]);
  if (loading) return <p>Loading...</p>;
  return (
    <div className={`${styles.rooms}  ${isOpenBurger ? styles.rooms_open : ""} `}>
      <div className={styles.rooms_header}>
        <div className={styles.rooms_header_title}>Rooms</div>
        <div className={styles.rooms_header_button}>
          <button
            onClick={() => setIsActiveAddRoom(!isActiveAddRoom)}
            className={`${styles.button_plus} ${isActiveAddRoom ? styles.close_button : ''}`}
          >
            <svg width="16" height="16" viewBox="0 0 16 16">
              <line x1="0" y1="8" x2="16" y2="8"></line>
              <line x1="8" y1="0" x2="8" y2="16"></line>
            </svg>
          </button>
        </div>
      </div>
      {isActiveAddRoom ? (
        <AddRoomField setIsActiveAddRoom={setIsActiveAddRoom} />
      ) : (
        conversations.map((conversation: IConversation) => {
          return (
            <Room
              onClick={onClick}
              key={conversation.id}
              name={conversation.name}
              id={conversation.id}
              activeRoomId={activeRoomId}
            />
          );
        })
      )}
    </div>
  );
};

export default Rooms;
