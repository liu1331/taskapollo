import React, { FC, useState } from "react";
import Button from "../../Button/Button";
import Input from "../../Input/Input";
import styles from "./addRoomField.module.scss";
import { CREATE_CONVERSATION } from "../../../graphql/mutation";
import { useMutation } from "@apollo/client";

interface IAddRoomField {
  setIsActiveAddRoom: (isActive: boolean) => void;
}

const AddRoomField: FC<IAddRoomField> = ({ setIsActiveAddRoom }) => {
  const [conversationName, setConversationName] = useState("");
  const [createConversation] = useMutation(CREATE_CONVERSATION);

  const onClick = () => {
    if (!conversationName) return;
    createConversation({ variables: { name: conversationName } });
    setIsActiveAddRoom(false);
  };
  return (
    <div className={`${styles.addRoomField}`}>
      <Input
        value={conversationName}
        onChange={(value: string) => setConversationName(value)}
        name="conversation"
        title="Conversation"
        placeholder="Name"
      />
      <div className={`${styles.addRoomField_button}`}>
        <Button onClick={() => onClick()} title="Add" />
      </div>
    </div>
  );
};

export default AddRoomField;
