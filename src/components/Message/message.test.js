import * as React from "react";
import { shallow } from "enzyme";
import Message from "./Message";
import toJson from "enzyme-to-json";

import Enzyme from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

Enzyme.configure({ adapter: new Adapter() });

describe("message snapshots", () => {
  it("message render without isMyMessage", () => {
    const message = toJson(shallow(<Message />));
    expect(message).toMatchSnapshot();
  });
  it("message snapshot with isMyMessage === false", () => {
    const message = shallow(<Message isMyMessage={false} />);
    expect(toJson(message)).toMatchSnapshot();
  });
});
describe("class interlocutor_message", () => {
  it("class interlocutor_message without props", () => {
    const message = shallow(<Message />);
    expect(message.find(".message").hasClass("interlocutor_message")).toEqual(
      false
    );
  });
  it("class interlocutor_message with userId !== convId props", () => {
    const message = shallow(<Message isMyMessage={false} />);
    expect(message.find(".message").hasClass("interlocutor_message")).toEqual(
      true
    );
    const message2 = shallow(<Message isMyMessage={true} />);
    expect(message2.find(".message").hasClass("interlocutor_message")).toEqual(
      false
    );
  });
});

describe("message text", () => {
  it("text with props message", () => {
    const message = shallow(<Message description={"React"} />);
    expect(message.find("p").first().text()).toEqual("React");
  });
  it("text without props message", () => {
    const message = shallow(<Message />);
    expect(message.find("p").first().text()).toEqual("");
  });
});
