import React, { FC, useMemo } from "react";
import { IUser } from "../../hooks/AuthContext";
import formatDataFunction from '../../utils/formatDataFunction';
import Avatar from "../Avatar/Avatar";
import styles from "./message.module.scss";
interface IMessage {
  isMyMessage?: boolean;
  description?: string;
  date?: Date;
  user?: IUser;
}
const Message: FC<IMessage> = ({
  isMyMessage = true,
  description,
  date,
  user,
}) => {
  const messageDate = useMemo(() => {
    if(date){
      const { day, month, year, hours, minutes } = formatDataFunction(date);
      return `${day}.${month < 10 ? "0"+month : month}.${year} ${hours}:${minutes < 10 ? "0"+minutes : minutes}`;
    }
  }, []);

  return (
    <div
      className={`${styles.message} ${
        isMyMessage ? "" : styles.interlocutor_message
      }`}
    >
      <div className={`${styles.message_avatar}`}>
        <Avatar src={user?.avatar} />
      </div>
      <div className={`${styles.message_text}`}>
        <div className={styles.description}>
          <p>{description}</p>
        </div>
        <div className={styles.massage_date}>{<p>{messageDate}</p>}</div>
      </div>
    </div>
  );
};

export default Message;
