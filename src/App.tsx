import { Navigate, Route, Routes } from "react-router-dom";
import "./App.scss";
import Header from "./components/Header/Header";
import Layout from "./components/Layout/Layout";
import { useAuthContext } from "./hooks/AuthContext";
import { IRoute, PrivateRoutes, PublicRoutes } from "./route/routes";

function App() {
  const { user } = useAuthContext();

  return (
    <div className="App">
      <Header />
      <Layout>
        {user.isAuth ? (
          <Routes>
            {PrivateRoutes.map((route: IRoute) => (
              <Route
                path={route.path}
                element={route.element}
                key={route.path}
              />
            ))}
            <Route path="*" element={<Navigate to="/" />} key="1" />
          </Routes>
        ) : (
          <Routes>
            {PublicRoutes.map((route: IRoute) => (
              <Route
                path={route.path}
                element={route.element}
                key={route.path}
              />
            ))}
            <Route path="*" element={<Navigate to="/login" key="2" />} />
          </Routes>
        )}
      </Layout>
    </div>
  );
}

export default App;
