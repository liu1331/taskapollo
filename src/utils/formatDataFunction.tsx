import React from "react";

interface IFormatData {
  year: number;
  month: number;
  day: number;
  hours: number;
  minutes: number;
  seconds: number;
  milliseconds: number;
}

const formatDataFunction = (data: Date): IFormatData => {
  return {
    year: data.getFullYear(),
    month: data.getMonth()+1,
    day: data.getDate(),
    hours: data.getHours(),
    minutes: data.getMinutes(),
    seconds: data.getSeconds(),
    milliseconds: data.getMilliseconds(),
  };
};

export default formatDataFunction;
