const re =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const emailValidation = {
  validEmail: (value: string) => {
    if (value.length !== 0) {
      if (!re.test(String(value).toLowerCase())) {
        return { errData: "Не корректный email" };
      } else {
        return { errData: "" };
      }
    } else {
      return { errData: "" };
    }
  },
};
export const passwordValidation = {
  passwordIsEmpty: (value: string) => {
    if (value.length === 0) {
      return { errData: "Введите пароль" };
    } else {
      return { errData: "" };
    }
  },
  passwordLength: (value: string) => {
    return function (from: number = 0, to: number = 100) {
      if (value.length < from || value.length > to) {
        return {
          errData: `Пароль должен содержать от ${from} до ${to} символов`,
        };
      } else {
        return { errData: "" };
      }
    };
  },
  isRepetedPassword: (password: string, repeatPassword: string) => {
    if (repeatPassword !== password) {
      return { errData: "Пароль не совпадает" };
    } else {
      return { errData: "" };
    }
  },
};
