import {
  BubbleDataPoint,
  ChartConfiguration,
  ChartData,
  ChartOptions,
  ChartType,
  ScatterDataPoint,
} from "chart.js/auto";

interface IUseCreateGraphConfig {
  graphType?: ChartType;
  statistics: Array<number | ScatterDataPoint | BubbleDataPoint | null>;
}

interface IConfig {
  type: ChartType;
  data: ChartData;
  options?: ChartOptions;
}
const createGraphConfig = ({
  graphType = "bar",
  statistics,
}: IUseCreateGraphConfig): IConfig | undefined => {
  if (statistics.length === 0) return;
  const graphConfig: ChartConfiguration = {
    type: graphType,
    data: {
      labels: [
        "7 days ago",
        "6 days ago",
        "5 days ago",
        "4 days ago",
        "3 days ago",
        "Yesterday",
        "Today",
      ],
      datasets: [
        {
          label: "Your message statistic",
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(201, 203, 207, 0.2)",
          ],
          data: [...statistics],
          borderColor: [
            "rgb(255, 99, 132)",
            "rgb(255, 159, 64)",
            "rgb(255, 205, 86)",
            "rgb(75, 192, 192)",
            "rgb(54, 162, 235)",
            "rgb(153, 102, 255)",
            "rgb(201, 203, 207)",
          ],
          borderWidth: 2,
        },
      ],
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        legend: {
          display: false,
        },
      },
    },
  };
  return graphConfig;
};

export default createGraphConfig;
