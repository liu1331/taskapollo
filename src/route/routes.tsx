import React, { Suspense } from 'react';
import ChatPage from "../pages/ChatPage/ChatPage";
import LoginPage from "../pages/LoginPage/LoginPage";
const ProfilePage = React.lazy(() => import("../pages/ProfilePage/ProfilePage"));
const RegistrationPage = React.lazy(() => import("../pages/RegistrationPage/RegistrationPage"));


export interface IRoute {
  path: string;
  element: React.ReactElement;
  key?: string;
}

export enum RouteNames {
  CHAT = "/",
  LOGIN = "/login",
  REGISTRATION = "/registration",
  PROFILE = "/profile",
}
export const PrivateRoutes: IRoute[] = [
  { path: RouteNames.CHAT, element: <ChatPage /> },
  { path: RouteNames.PROFILE, element: <Suspense fallback={<div>Loading...</div>}><ProfilePage /> </Suspense> },
];
export const PublicRoutes: IRoute[] = [
  { path: RouteNames.LOGIN, element: <LoginPage /> },
  { path: RouteNames.REGISTRATION, element: <Suspense fallback={<div>Loading...</div>}><RegistrationPage /></Suspense> },
];
