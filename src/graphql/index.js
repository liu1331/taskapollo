import {
  ApolloClient,
  from,
  HttpLink,
  InMemoryCache,
  ApolloLink,
  split,
} from "@apollo/client";
import { getMainDefinition } from "@apollo/client/utilities";
import { WebSocketLink } from "apollo-link-ws";

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers = {} }) => ({
    headers: {
      ...headers,
      "access-token": localStorage.getItem("token") || null,
    },
  }));

  return forward(operation);
});
const httpLink = new HttpLink({
  uri: process.env.REACT_APP_SERVER_URI,
});
export const wsLink = new WebSocketLink({
  uri: process.env.REACT_APP_WS_URI,
  options: {
    lazy: true,
    reconnect: true,
    connectionParams: () => {
      return {
        "access-token": localStorage.getItem("token"),
      };
    },
  },
  connectionCallback: (error) => {
    if (error) {
      console.log(error);
    }
  },
});

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query);
    return (
      definition.kind === "OperationDefinition" &&
      definition.operation === "subscription"
    );
  },
  wsLink,
  httpLink
);
export const reconnectSubscriptionClient = () => {
  wsLink.subscriptionClient.close();
};

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: from([authMiddleware, splitLink]),
});
