import gql from "graphql-tag";
export const SIGN_IN = gql`
  query signIn($password: String!, $email: String!) {
    signIn(password: $password, email: $email) {
      user {
        id
        login
        email
        avatar
      }
      token
    }
  }
`;
export const ME = gql`
  query me {
    me {
      user {
        id
        login
        email
        avatar
      }
      token
    }
  }
`;
export const GET_ALL_CONVERSATIONS = gql`
  query getAllConversations {
    getAllConversations {
      id
      createdBy
      name
      date
    }
  }
`;
export const GET_ALL_MESSAGES = gql`
  query getAllMessages($convId: Int) {
    getAllMessages(convId: $convId) {
      id
      description
      userId
      convId
      date
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;
export const GET_MESSAGES_STATISTICS = gql`
  query getMessageStatistics {
    getMessageStatistics {
      count
      date
    }
  }
`;
export const TYPING_USER = gql`
  query typingUser($convId: Int!) {
    typingUser(convId: $convId) {
      convId
      date
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;
