import gql from "graphql-tag";

export const REGISTRATION = gql`
  mutation registr(
    $avatar: String!
    $email: String!
    $password: String!
    $login: String!
  ) {
    registration(
      avatar: $avatar
      email: $email
      password: $password
      login: $login
    ) {
      user {
        id
        login
        email
        avatar
      }
      token
    }
  }
`;
export const CREATE_MESSAGE = gql`
  mutation createMessage($convId: Int!, $description: String!) {
    createMessage(convId: $convId, description: $description) {
      id
      description
      userId
      convId
      date
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;
export const CREATE_CONVERSATION = gql`
  mutation createConversation($name: String!) {
    createConversation(name: $name) {
      id
      createdBy
      name
      date
    }
  }
`;
