import { gql } from "@apollo/client";

export const MESSAGE_ADDED = gql`
  subscription messageAdded($date: DateTime!) {
    messageAdded(date: $date) {
      id
      description
      userId
      convId
      date
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;
export const CONVERSATION_ADDED = gql`
  subscription conversationAdded {
    conversationAdded {
      id
      createdBy
      name
      date
    }
  }
`;
export const IS_TYPING = gql`
  subscription isTyping($convId: Float!) {
    isTyping(convId: $convId) {
      convId
      date
      user {
        id
        login
        email
        avatar
      }
    }
  }
`;
