module.exports = {
  env: {
    test: {
      presets: [
        [
          "@babel/preset-env",
          {
            modules: "commonjs",
            debug: false,
          },
        ],
        "@babel/preset-react",
        "@babel/preset-typescript",
      ],
    },
    production: {
      presets: [
        ["@babel/preset-env", { modules: false }],
        "@babel/preset-react",
        "@babel/preset-typescript",
      ],
    },
    development: {
      presets: [
        ["@babel/preset-env", { modules: false }],
        "@babel/preset-react",
        "@babel/preset-typescript",
      ],
    },
  },
};
